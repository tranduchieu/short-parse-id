Short Parse ID
================

## Installation

  `npm install @tranduchieu/short-parse-id`

## Usage

  ```
  import ShortParseId from '@tranduchieu/short-parse-id';
  const ShortId = new ShortParseId(
    ID_LENGTH, // default is 6
    APP_ID,
    SERVER_URL,
  );
  ```

### Batch add IDs
  ```
  ShortId.batchAdd(5)
  .then(console.log)
  .catch(console.error);
  ```

### Get an ID
  ```
  ShortId.getOne
  .then(console.log)
  .catch(console.error);
  ```
  Output: `JFH-8C8`

### Status
  ```
  ShortId.status()
  .then(console.log)
  .catch(console.error);
  ```
  Output:
  ```
  {
    available: 12,
    used: 78
  }
  ```

## Tests

  `npm test`

## Contributing

In lieu of a formal style guide, take care to maintain the existing coding style. Add unit tests for any new or changed functionality. Lint and test your code.