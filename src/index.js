import Parse from 'parse/node';
import _ from 'lodash';

const SERVER_PORT = process.env.PORT || 8080;

class ShortParseId {
  constructor(idLength = 6, appId, serverUrl) {
    this.ALPHABET = '23456789abdegjkmnpqrvwxyz';
    this.ALPHABET_LENGTH = this.ALPHABET.length;
    this.ID_LENGTH = idLength;

    Parse.initialize(appId || process.env.APP_ID || null);
    Parse.serverURL = serverUrl || `http://localhost:${SERVER_PORT}/parse`;
    this.Parse = Parse;
  }

  generateId() {
    let rtn = '';
    for (let i = 0; i < this.ID_LENGTH; i++) {
      rtn += this.ALPHABET.charAt(Math.floor(Math.random() * this.ALPHABET_LENGTH));
    }
    rtn = [rtn.slice(0, 3), '-', rtn.slice(3)].join('');
    return rtn;
  }

  checkUniqueAndCreate(id) {
    const ShortId = this.Parse.Object.extend('ShortId');
    const query = new this.Parse.Query(ShortId);
    query.equalTo('shortId', id);
    return query.first()
    .then(shortIdObj => {
      if (shortIdObj) return Promise.reject('shortId đã tồn tại');
      const shortId = new ShortId();
      shortId.set('shortId', id);
      shortId.set('used', false);
      return shortId.save();
    });
  }

  batchAdd(times) {
    return _.times(times, async () => {
      let duplicate = true;

      do {
        const id = this.generateId();
        let checkUnique;
        try {
          checkUnique = await this.checkUniqueAndCreate(id);
          duplicate = false;
          console.log(checkUnique);
        } catch (error) {
          console.error(error);
        }
      }
      while (duplicate === true);
    });
  }

  async getOne(sessionToken) {
    const ShortId = this.Parse.Object.extend('ShortId');
    const query = new this.Parse.Query(ShortId);
    query.equalTo('used', false);
    const shortIdClass = await query.first({ sessionToken });
    if (!shortIdClass) throw new Error('Không tìm thấy shortId nào còn khả dụng');
    const id = shortIdClass.get('shortId');

    // Set là đã dùng
    shortIdClass.set('used', true);
    await shortIdClass.save(null, { sessionToken });

    return id;
  }

  availableCount() {
    const ShortId = this.Parse.Object.extend('ShortId');
    const query = new this.Parse.Query(ShortId);
    query.equalTo('used', false);
    return query.count();
  }

  usedCount() {
    const ShortId = this.Parse.Object.extend('ShortId');
    const query = new this.Parse.Query(ShortId);
    query.equalTo('used', true);
    return query.count();
  }

  status() {
    return Promise.all([
      this.availableCount(),
      this.usedCount(),
    ])
    .then(([available, used]) => {
      return { available, used };
    });
  }
}

export default ShortParseId;

// const ShortId = new ShortParseId(6, 'myAppId');
// ShortId.checkUniqueAndCreate('JDG-8D9')
// .then(console.log)
// .catch(console.error);

// ShortId.batchAdd(5);
// ShortId.getOne('r:8730f50985bb6174b7451299af1ced8e')
// .then(console.log)
// .catch(console.error);

// ShortId.status()
// .then(console.log)
// .catch(console.error);

// console.log(ShortId.getOne('r:8730f50985bb6174b7451299af1ced8e'));
